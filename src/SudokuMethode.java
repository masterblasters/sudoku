package sudokuSolver;

import java.io.*;
import java.util.Arrays;
import java.util.Vector;

public class SudokuMethode{
     
	public SudokuMethode(){}
	
	public void createSudokuField(){ //Initialize
		
		sud = new String [][]
			   
			  {{"123456789", "123456789", "123456789", "123456789", "123456789",  //0. line // Zero row  
		        "123456789", "123456789", "123456789", "123456789"},
		     
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //1. line // Second row
		        "123456789", "123456789", "123456789", "123456789"},
		      
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //2. line // Second row
		        "123456789", "123456789", "123456789", "123456789"},
		     
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //3. line // Third row
		        "123456789", "123456789", "123456789", "123456789"},
		                 
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //4. line // Fourth row
		        "123456789", "123456789", "123456789", "123456789"},
		        
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //5. line // Fifth row
		        "123456789", "123456789", "123456789", "123456789"},
		         
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //6. line // Sixth row
		        "123456789", "123456789", "123456789", "123456789"},
		          
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //7. line // Seventh row
		        "123456789", "123456789", "123456789", "123456789"},
		         
		       {"123456789", "123456789", "123456789", "123456789", "123456789",  //8. line // Eighth row 
		        "123456789", "123456789", "123456789", "123456789"}};
	}
	
	public void printSudoku(){
		for(String [] klaus : sud){
			System.out.println(Arrays.toString(klaus));
		}
	System.out.println("");
	}
	
	public void setGUISudoku(String[][] sudoku) {
		
		for (int line_index = 0; line_index < sudoku.length; line_index++) {
			for (int col_index = 0; col_index < sudoku[0].length; col_index++) {
				try{
					if (!sudoku[line_index][col_index].equals("")){
						sud[line_index][col_index] = sudoku[line_index][col_index];
					}
				} catch(Exception E) {
					System.out.println("Error: Strange Null Pointer row: " + line_index + " column: " + col_index);
				}
			}
		}
	}

	public void setTxtSudoku() throws Exception{ //Show me your Sudoku
		
                @SuppressWarnings("resource")
				BufferedReader br = new BufferedReader(new FileReader("sudoku.txt"));
		
                int line = 0;

                while(br.ready()){

                	String line_sequence = br.readLine().replace("-", "").replace(" ", "");
   
                	if (line_sequence.charAt(0) == '_') {
                		continue;
                	}
                	
                	String[] line_content = line_sequence.split("(?!^)");              
                	
                    for(int i = 0; i < line_content.length; i++) {

                    	if(!line_content[i].equals("x")){
                    		sud[line][i] = line_content[i];
                        }
                    }
                    line++;
                }
	}
	
	public void setSudoku() throws Exception{ //Show me your Sudoku
		
		
		for(int r = 1; r < 10; r++){ //R is counting rows
		
		System.out.println("How many numbers are in line " + r + "?"); // Shown in Syso
		BufferedReader Br4 = new BufferedReader(new InputStreamReader(System.in));
		int j = Integer.parseInt(Br4.readLine());
		
		for(int k = 0; k < j; k++){
		
		System.out.println("Which column?");		//Shown in Syso
		BufferedReader Br1 = new BufferedReader(new InputStreamReader(System.in));
		int c = Integer.parseInt(Br1.readLine());
		
		System.out.println("Which number?");   //Shown in Syso
		BufferedReader Br3 = new BufferedReader(new InputStreamReader(System.in));
		String n = Br3.readLine();
		
		sud[r - 1][c - 1] = n; //Convert to machine definition of arrays
		}
		}
	}
	
	public String[][] getSudoku() {
		return sud;
	}
	
	public void setSudokuField(String[][] sudoku_field) {
		for(int row = 0; row < 9; row++) {
			for(int col = 0; col < 9; col++){
				sud[row][col] = sudoku_field[row][col];
			}
		}
	}

	
	public void checkSudoku() throws Exception{
		for(int g = 0; g < 9; g++) {      //(Proof the whole width and
			for(int z = 0; z < 9; z++){   //(the whole height of the 2D array
				if(Integer.parseInt(sud[g][z]) < 10){
					for(int w = 0; w < g; w++){  //Check for every column if there are Numbers impossible
						for(int k = 0; k < sud[w][z].length(); k++){
							if(sud[g][z].charAt(0) == sud[w][z].charAt(k)){
								sud[w][z] = sud[w][z].substring(0,k).concat(sud[w][z].substring(k + 1, sud[w][z].length()));
					           }
						}   // Split in two parts (divide & conquer)
					}
					for(int w = g + 1; w < 9; w++){
						for(int k = 0; k < sud[w][z].length(); k++){
							if(sud[g][z].charAt(0) == sud[w][z].charAt(k)){
								sud[w][z] = sud[w][z].substring(0,k).concat(sud[w][z].substring(k + 1, sud[w][z].length()));
					           }
						}
					}	
					for(int w = 0; w < z; w++){   // The same for every row/line
						for(int k = 0; k < sud[g][w].length(); k++){
							if(sud[g][z].charAt(0) == sud[g][w].charAt(k)){
								sud[g][w] = sud[g][w].substring(0,k).concat(sud[g][w].substring(k + 1, sud[g][w].length()));
					           }
						}
					}
					for(int w = z + 1; w < 9; w++){
						for(int k = 0; k < sud[g][w].length(); k++){
							if(sud[g][z].charAt(0) == sud[g][w].charAt(k)){
								sud[g][w] = sud[g][w].substring(0,k).concat(sud[g][w].substring(k + 1, sud[g][w].length()));
					           }
						}
					}	
				}
			}
	   }
		for(int i = 0; i < 3; i++){    //Check the squares
		  for(int q = 0; q < 3; q++){
			  if(Integer.parseInt(sud[i][q]) < 10){
                 for(int z = 0; z < 3; z++){
                	 for(int u = 0; u < 3; u++){
                		 if(z != i || u != q){
                			 for(int k = 0; k < sud[z][u].length(); k++){
                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
                				 }
                		 }
                	 }
                 }
		  }
		}
		}
		}
		for(int i = 0; i < 3; i++){
			  for(int q = 3; q < 6; q++){
				  if(Integer.parseInt(sud[i][q]) < 10){
	                 for(int z = 0; z < 3; z++){
	                	 for(int u = 3; u < 6; u++){
	                		 if(z != i || u != q){
	                			 for(int k = 0; k < sud[z][u].length(); k++){
	                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
	                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
	                				 }
	                		 }
	                	 }
	                 }
			  }
			}
			}
			}
		for(int i = 0; i < 3; i++){
			  for(int q = 6; q < 9; q++){
				  if(Integer.parseInt(sud[i][q]) < 10){
	                 for(int z = 0; z < 3; z++){
	                	 for(int u = 6; u < 9; u++){
	                		 if(z != i || u != q){
	                			 for(int k = 0; k < sud[z][u].length(); k++){
	                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
	                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
	                				 }
	                		 }
	                	 }
	                 }
			  }
			}
			}
			}
		for(int i = 3; i < 6; i++){
			  for(int q = 0; q < 3; q++){
				  if(Integer.parseInt(sud[i][q]) < 10){
	                 for(int z = 3; z < 6; z++){
	                	 for(int u = 0; u < 3; u++){
	                		 if(z != i || u != q){
	                			 for(int k = 0; k < sud[z][u].length(); k++){
	                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
	                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
	                				 }
	                		 }
	                	 }
	                 }
			  }
			}
			}
			}
			for(int i = 3; i < 6; i++){
				  for(int q = 3; q < 6; q++){
					  if(Integer.parseInt(sud[i][q]) < 10){
		                 for(int z = 3; z < 6; z++){
		                	 for(int u = 3; u < 6; u++){
		                		 if(z != i || u != q){
		                			 for(int k = 0; k < sud[z][u].length(); k++){
		                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
		                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
		                				 }
		                		 }
		                	 }
		                 }
				  }
				}
				}
				}
			for(int i = 3; i < 6; i++){
				  for(int q = 6; q < 9; q++){
					  if(Integer.parseInt(sud[i][q]) < 10){
		                 for(int z = 3; z < 6; z++){
		                	 for(int u = 6; u < 9; u++){
		                		 if(z != i || u != q){
		                			 for(int k = 0; k < sud[z][u].length(); k++){
		                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
		                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
		                				 }
		                		 }
		                	 }
		                 }
				  }
				}
				}
				}
			for(int i = 6; i < 9; i++){
				  for(int q = 0; q < 3; q++){
					  if(Integer.parseInt(sud[i][q]) < 10){
		                 for(int z = 6; z < 9; z++){
		                	 for(int u = 0; u < 3; u++){
		                		 if(z != i || u != q){
		                			 for(int k = 0; k < sud[z][u].length(); k++){
		                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
		                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
		                				 }
		                		 }
		                	 }
		                 }
				  }
				}
				}
				}
				for(int i = 6; i < 9; i++){
					  for(int q = 3; q < 6; q++){
						  if(Integer.parseInt(sud[i][q]) < 10){
			                 for(int z = 6; z < 9; z++){
			                	 for(int u = 3; u < 6; u++){
			                		 if(z != i || u != q){
			                			 for(int k = 0; k < sud[z][u].length(); k++){
			                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
			                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
			                				 }
			                		 }
			                	 }
			                 }
					  }
					}
					}
					}
				for(int i = 6; i < 9; i++){
					  for(int q = 6; q < 9; q++){
						  if(Integer.parseInt(sud[i][q]) < 10){
			                 for(int z = 6; z < 9; z++){
			                	 for(int u = 6; u < 9; u++){
			                		 if(z != i || u != q){
			                			 for(int k = 0; k < sud[z][u].length(); k++){
			                				 if(sud[i][q].charAt(0) == sud[z][u].charAt(k)){
			                					 sud[z][u] = sud[z][u].substring(0,k).concat(sud[z][u].substring(k + 1, sud[z][u].length()));
			                				 }
			                		 }
			                	 }
			                 }
					  }
					}
					}
			}
	boolean b = true;
	  for(int m = 0; m < 9; m++){  //Are there numbers which exists only in one column of the row?
	  for(int w = 0; w < 9; w++){ 
	    for(int o = 0; o < sud[m][w].length(); o++){
          for(int i = 0; i < 9; i++){
	    	  for(int z = 0; z < sud[m][i].length(); z++){		
	    		  if((sud[m][w].charAt(o) == sud[m][i].charAt(z)) && (w != i)){
					b = false;    	
    }
    }
    }
	if(b) {
		sud[m][w] = sud[m][w].substring(o, o + 1);
		}
	b = true;
	    }
	  }
	}
	
	b = true;
	for(int m = 0; m < 9; m++){ //Are there numbers which exists only in one row of the column?
		  for(int w = 0; w < 9; w++){ 
		    for(int o = 0; o < sud[w][m].length(); o++){
	          for(int i = 0; i < 9; i++){
		    	  for(int z = 0; z < sud[i][m].length(); z++){		
		    		  if((sud[w][m].charAt(o) == sud[i][m].charAt(z)) && (w != i)){
						b = false;    	
	    }
	    }
	    }
		if(b) {
			sud[w][m] = sud[w][m].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	
	b = true;  // Do it for the squares
	for(int m = 0; m < 3; m++){
		  for(int w = 0; w < 3; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 0; i < 3; i++){
	        	  for(int g = 0; g < 3; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 0; m < 3; m++){
		  for(int w = 3; w < 6; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 0; i < 3; i++){
	        	  for(int g = 3; g < 6; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 0; m < 3; m++){
		  for(int w = 6; w < 9; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 0; i < 3; i++){
	        	  for(int g = 6; g < 9; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 3; m < 6; m++){
		  for(int w = 0; w < 3; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 3; i < 6; i++){
	        	  for(int g = 0; g < 3; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 3; m < 6; m++){
		  for(int w = 3; w < 6; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 3; i < 6; i++){
	        	  for(int g = 3; g < 6; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 3; m < 6; m++){
		  for(int w = 6; w < 9; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 3; i < 6; i++){
	        	  for(int g = 6; g < 9; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 6; m < 9; m++){
		  for(int w = 0; w < 3; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 6; i < 9; i++){
	        	  for(int g = 0; g < 3; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 6; m < 9; m++){
		  for(int w = 3; w < 6; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 6; i < 9; i++){
	        	  for(int g = 3; g < 6; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	b = true;
	for(int m = 6; m < 9; m++){
		  for(int w = 6; w < 9; w++){ 
		    for(int o = 0; o < sud[m][w].length(); o++){
	          for(int i = 6; i < 9; i++){
	        	  for(int g = 6; g < 9; g++){
		    	    for(int z = 0; z < sud[i][g].length(); z++){		
		    		    if((sud[m][w].charAt(o) == sud[i][g].charAt(z)) && ((m != i) || (w != g))){
						  b = false;    	
	    }
	    }
	    }
	    }
		if(b) {
			sud[m][w] = sud[m][w].substring(o, o + 1);
			}
		b = true;
		    }
		  }
		}
	} 
	
	
	public void bruteForceSystematic() {
		
		Vector<SudokuMethode> possible_sudokus = new Vector<SudokuMethode>();
		int backtrack_counter = 0;
		
		SudokuMethode sudoku_origin = new SudokuMethode();		
		sudoku_origin.createSudokuField();
		sudoku_origin.setSudokuField(this.getSudoku());	
		possible_sudokus.add(sudoku_origin);
		
	
		
		for(int sudoku_index = 0; sudoku_index < possible_sudokus.size(); sudoku_index++) {
		
			for (int row = 0; row < 9; row++) {
				for (int col = 0; col < 9; col++) {
					
					if (possible_sudokus.elementAt(sudoku_index).getSudoku()[row][col].length() > 1) {
						
						SudokuMethode[] sudoku_options = new SudokuMethode[possible_sudokus.elementAt(sudoku_index).getSudoku()[row][col].length()];
						
						for (int char_index = 0; char_index < sudoku_options.length; char_index++) {
							sudoku_options[char_index] = new SudokuMethode();
							sudoku_options[char_index].createSudokuField();
							sudoku_options[char_index].setSudokuField(possible_sudokus.elementAt(sudoku_index).getSudoku());
							sudoku_options[char_index].setSpecificEntry(row, col, possible_sudokus.elementAt(sudoku_index).getSudoku()[row][col].substring(char_index, char_index + 1 ));
						}
						
						possible_sudokus.remove(sudoku_index);
						
						for (int char_index = 0; char_index < sudoku_options.length; char_index++) {
						
							try{
								for (int r = 0; r < 81; r++){
									sudoku_options[char_index].checkSudoku();
								}
								boolean isFinished = true;
								for (int found_row = 0; found_row < 9; found_row++) {
									for (int found_col = 0; found_col < 9; found_col++) {
										if (sudoku_options[char_index].getSudoku()[found_row][found_col].length() > 1) {
											isFinished = false;
										}
									}
								}
								if (isFinished) {
									System.out.println("is finished");
									sud = sudoku_options[char_index].getSudoku();
									return;
								}
								possible_sudokus.insertElementAt(sudoku_options[char_index], 0);
								
							} catch (Exception E){								
									
								backtrack_counter++;
								System.out.println("Backtrack: " + backtrack_counter);
								row = 10;
								col = 10;
								sudoku_index = -1;
								continue;
									
							}
						}
					}
						
				}
			}
		}
		
	}
	
	
	public void setSpecificEntry(int row, int col, String entry) {
		
		sud[row][col] = entry;
		
	}
	
	public void bruteForceRandom() {
	
		String[][] my_sudoku = new String[9][9];
		
		for(int row = 0; row < 9; row++) {
			for(int col = 0; col < 9; col++){
				my_sudoku[row][col] = sud[row][col];
			}
		}
		
		int backtrack_counter = 0;
		
		for(int row = 0; row < 9; row++) {
			for(int col = 0; col < 9; col++){
					
				if (sud[row][col].length() > 1) {
					
					int my_rand_int = (int) (Math.random() * sud[row][col].length());
					
					System.out.println(my_rand_int);
					
					sud[row][col] = sud[row][col].substring(my_rand_int, my_rand_int + 1);
						try{
							for(int rep = 0; rep < 81; rep++){
								this.checkSudoku();
							}
						} catch(Exception e){
							
							for(int row2 = 0; row2 < 9; row2++) {
								for(int col2 = 0; col2 < 9; col2++){
									sud[row2][col2] = my_sudoku[row2][col2];
								}
							}
							row = 0;
							col = -1;
							backtrack_counter++;
							System.out.println("Backtrack: " + backtrack_counter);
							continue;
						}
				}
			}
		
		}
		
	}
	
	private String[][] sud;
}