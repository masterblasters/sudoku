package sudokuSolver;

import java.awt.Color;
import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SudokuGUI{
	
	private JFrame mWindow;
	private String[][] mSudoku_field = new String[9][9];
	
	SudokuGUI() {
		mWindow = new JFrame("Sudoku");
		
		mWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mWindow.setLocation(0, 0);
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int screen_width = gd.getDisplayMode().getWidth();
		int screen_height = gd.getDisplayMode().getHeight();
		mWindow.setSize(screen_width, screen_height);
		mWindow.setResizable(true);
	}
	
	SudokuGUI(int width, int height) {
		mWindow = new JFrame("Sudoku");
		
		mWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mWindow.setLocation(0, 0);
		
		mWindow.setSize(width, height);
		mWindow.setResizable(true);
	}
	
	public void generateSudokuField() throws Exception{
		
		mWindow.setLayout(new GridBagLayout());
		
		final JPanel sudoku_field_panel = new JPanel(new GridLayout(9, 9, 9, 9));
		
		final JTextField[][] sudoku_text_field = new JTextField[9][9];

		for (int line_index = 0; line_index < sudoku_text_field.length; line_index++) {
			for (int col_index = 0; col_index < sudoku_text_field[0].length; col_index++) {
				sudoku_text_field[line_index][col_index] = new JTextField("", 2);
				sudoku_text_field[line_index][col_index].setBackground(Color.WHITE);
				sudoku_text_field[line_index][col_index].setForeground(Color.BLACK);
				sudoku_field_panel.add(sudoku_text_field[line_index][col_index]);
			}
		}
		
		ActionListener solver_button = new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				Component[] text_fields = sudoku_field_panel.getComponents();
				
				for (int line_index = 0; line_index < mSudoku_field.length; line_index++) {
					for (int col_index = 0; col_index < mSudoku_field[0].length; col_index++) {
						mSudoku_field[line_index][col_index] = ((JTextField) text_fields[line_index * 9 + col_index]).getText();
					}
				}
				
				mWindow.dispose();
			}
		};
		
		
		JButton OkButton = new JButton("Solve Sudoku");
		
		OkButton.addActionListener(solver_button);
		
		JPanel button_panel = new JPanel();
		button_panel.add(OkButton);
		
		mWindow.add(sudoku_field_panel);
		mWindow.add(button_panel);
		
	}
	
	boolean is_active() {
		return mWindow.isActive();
	}
	
	boolean is_visible() {
		return mWindow.isVisible();
	}
		
	String[][] get_sudoku() {
		return mSudoku_field;
	}
	
	void show_sudoku(String[][] sudoku) {
		
		JFrame window = new JFrame("Sudoku");
		
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setLocation(0, 0);
		
		GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
		int screen_width = gd.getDisplayMode().getWidth();
		int screen_height = gd.getDisplayMode().getHeight();
		window.setSize(screen_width, screen_height);
		window.setResizable(true);
		
		JPanel sudoku_field_panel = new JPanel(new GridLayout(9, 9, 9, 9));
		
		final JTextField[][] sudoku_text_field = new JTextField[9][9];
				
		for (int line_index = 0; line_index < sudoku_text_field.length; line_index++) {
			for (int col_index = 0; col_index < sudoku_text_field.length; col_index++) {
				sudoku_text_field[line_index][col_index] = new JTextField(sudoku[line_index][col_index], 2);
				sudoku_text_field[line_index][col_index].setBackground(Color.WHITE);
				sudoku_text_field[line_index][col_index].setForeground(Color.BLACK);
				sudoku_field_panel.add(sudoku_text_field[line_index][col_index]);
			}
		}
		
		window.add(sudoku_field_panel);
		
		window.setVisible(true);
	};
	
	void Show_window() {
		mWindow.setVisible(true);
	}
}
