package sudokuSolver;


public class SudokuTxtMain{

	public static void main(String args[]) throws Exception{
	
		SudokuMethode S = new SudokuMethode();
	
		S.createSudokuField();
	
		S.printSudoku();
	
		S.setTxtSudoku();
	
		S.printSudoku();
	
		for(int r = 0; r < 81; r++){
			S.checkSudoku();
		}
		
		//S.bruteForce();
		S.bruteForceSystematic();
		
		S.printSudoku();
		
		System.out.println("Press Enter To Exit");
		
		System.in.read();
	
	}
}