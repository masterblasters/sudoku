package sudokuSolver;

public class SudokuGUIMain {

	public static void main(String[] args) throws Exception{
		
		SudokuMethode S = new SudokuMethode();
		S.createSudokuField();
		
		SudokuGUI gui = new SudokuGUI();
		gui.generateSudokuField();
		
		Thread.sleep(500);
		
		gui.Show_window();
	
		Thread.sleep(500);
		
		while(gui.is_active() || gui.is_visible()) {
			Thread.sleep(1000);
		}
		
		Thread.sleep(500);

		String[][] sudoku_entries = gui.get_sudoku();
		
		S.printSudoku();
		
		S.setGUISudoku(sudoku_entries);

		for (int r = 0; r < 81; r++){
			
			try{
				S.checkSudoku();
			}
			catch(Exception e) {
				System.out.println("Error: Invalid sudoku field.");
			}
		}
		
		//S.bruteForceRandom();
		S.bruteForceSystematic();
		
		gui.show_sudoku(S.getSudoku());
	}
	
}
