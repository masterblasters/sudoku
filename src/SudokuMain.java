package sudokuSolver;

public class SudokuMain{

	public static void main(String args[]) throws Exception{
	
		SudokuMethode S = new SudokuMethode();
	
		S.createSudokuField();
	
		S.printSudoku();
	
		S.setSudoku();
	
		S.printSudoku();
	
		for(int r = 0; r < 81; r++){
			S.checkSudoku();
		}
	
		//S.bruteForceRandom();
		S.bruteForceSystematic();
		
		S.printSudoku();
		
		System.out.println("Press Enter To Exit");
		
		System.in.read();
	
	
	}
}